import type { Component } from 'solid-js';
import { QueryClient, QueryClientProvider, createQuery } from '@tanstack/solid-query'
import { Switch, Match, For } from 'solid-js'
import {Container, ListGroup} from "solid-bootstrap";

const queryClient = new QueryClient()

import { Todo } from 'shared/src/shared';

async function fetchTodos(): Promise<Todo[]>{
  const result = await fetch("http://localhost:4000/api/todos");
  return await result.json() as Todo[];
}

function Example() {
  const query = createQuery(() => ['todos'], fetchTodos)

  return (
      <div>
        <Switch>
          <Match when={query.isLoading}>
            <p>Loading...</p>
          </Match>
          <Match when={query.isError}>
            <p>Error: {query.error.message}</p>
          </Match>
          <Match when={query.isSuccess}>
              <ListGroup>
            <For each={query.data}>
                {(todo) => <ListGroup.Item>{todo.title}</ListGroup.Item>}
            </For>
              </ListGroup>
          </Match>
        </Switch>
      </div>
  )
}

const App: Component = () => {
  return (
      <Container>
      <QueryClientProvider client={queryClient}>
        <Example />
      </QueryClientProvider>
      </Container>
  );
};

export default App;
