import express from 'express';
import cors from 'cors';
import {Todo} from "shared/src/shared";

const app = express();
app.use(cors());

const todos: Todo[] = [
    {
        title: "do shopping",
    },
    {
        title: "sweep floor",
    }
];

app.get('/api/todos', (req, res) => {
    res.json(todos);
})

app.listen(4000, () => console.log('listening on port 4000'));